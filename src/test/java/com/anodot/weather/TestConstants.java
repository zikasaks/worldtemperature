package com.anodot.weather;
public class TestConstants {

    public static final String MOSCOW = "MSK";
    public static final String TEL_AVIV = "TLV";
    public static final String HONK_KONG = "HKG";
    public static final String NEW_YORK = "NY";

    public static final String MAX_AGGREGATION = "max";
}
