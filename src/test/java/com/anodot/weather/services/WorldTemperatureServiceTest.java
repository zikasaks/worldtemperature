package com.anodot.weather.services;

import com.anodot.weather.TestConstants;
import com.anodot.weather.config.Config;
import com.anodot.weather.dto.AggregatedTemperature;
import com.anodot.weather.weatherSdk.external.WeatherAPI;
import com.anodot.weather.weatherSdk.external.dto.City;
import com.anodot.weather.weatherSdk.external.dto.DailyTemp;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static com.anodot.weather.TestConstants.MAX_AGGREGATION;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class WorldTemperatureServiceTest {

    @Mock
    Config config;
    @Mock
    WeatherAPI weatherAPI;
    private WorldTemperatureService worldTemperatureService;

    @BeforeEach
    void setService() {
        ExecutorService executorService = Executors.newFixedThreadPool(4);
        worldTemperatureService = new WorldTemperatureService(config, weatherAPI, executorService);
    }

    @Test
    void shouldBeReturnedZeroResultsWhenNoCitiesWithEnoughPopulation() throws ExecutionException, InterruptedException {
        Set<String> cities = Set.of(TestConstants.TEL_AVIV, TestConstants.MOSCOW, TestConstants.NEW_YORK, TestConstants.HONK_KONG);

        Set<City> citiesWithNotEnoughPopulation = getCitiesWithNotEnoughPopulation(cities);
        List<AggregatedTemperature> topAggregatedTemperatures = getResult(cities, citiesWithNotEnoughPopulation);

        cities.forEach((city) -> verify(weatherAPI, atMost(0)).getLastYearTemperature(city));

        assert topAggregatedTemperatures != null;
        assert topAggregatedTemperatures.isEmpty();
    }

    @Test
    void shouldBeReturnedOneResultWhenOnlyOneWithEnoughPopulation() throws ExecutionException, InterruptedException {
        Set<String> citiesId = Set.of(TestConstants.TEL_AVIV, TestConstants.MOSCOW);
        Set<City> cities = getCitiesWithNotEnoughPopulation(Set.of(TestConstants.TEL_AVIV));
        Set<City> mskCity = getCitiesWithEnoughPopulation(Set.of(TestConstants.MOSCOW));
        cities.addAll(mskCity);

        List<AggregatedTemperature> topAggregatedTemperatures = getResult(citiesId, cities);

        assert topAggregatedTemperatures != null;
        assert topAggregatedTemperatures.size() == 1;

        AggregatedTemperature aggregatedTemperature = topAggregatedTemperatures.get(0);

        assert Double.compare(aggregatedTemperature.getTemperature(), 43.5) == 0;
        assert TestConstants.MOSCOW.equals(aggregatedTemperature.getCity().getId());

    }

    @Test
    void shouldReturnOnlyThreeCountries() throws ExecutionException, InterruptedException {
        Set<String> cities = Set.of(TestConstants.TEL_AVIV,
                TestConstants.MOSCOW,
                TestConstants.NEW_YORK,
                TestConstants.HONK_KONG);
        Set<City> citiesWithEnoughPopulation = getCitiesWithEnoughPopulation(cities);

        List<AggregatedTemperature> topAggregatedTemperatures = getResult(cities, citiesWithEnoughPopulation);

        assert topAggregatedTemperatures != null;
        assert topAggregatedTemperatures.size() == 3;
    }

    private List<AggregatedTemperature> getResult(Set<String> cities, Set<City> citiesData) throws ExecutionException, InterruptedException {
        when(config.getIds()).thenReturn(cities);
        lenient().when(config.getAggregation()).thenReturn(MAX_AGGREGATION);
        when(weatherAPI.getAllCitiesByIds(cities)).thenReturn(citiesData);
        cities.forEach(city ->  lenient().when(weatherAPI.getLastYearTemperature(city)).thenReturn(getTestDailyTemp()));
        return worldTemperatureService.getTopAggregatedTemperatures();
    }

    private List<DailyTemp> getTestDailyTemp() {
        return List.of(new DailyTemp(new Date(), 10),
                new DailyTemp(new Date(), 24),
                new DailyTemp(new Date(), 43.5));
    }

    private Set<City> getCitiesWithNotEnoughPopulation(Set<String> cities) {
        return getCitiesWithPopulation(cities, 343);
    }

    private Set<City> getCitiesWithEnoughPopulation(Set<String> cities) {
       return getCitiesWithPopulation(cities, 3434544);
    }

    private Set<City> getCitiesWithPopulation(Set<String> cities, int population) {
        return cities.stream().map(
                (city) ->  new City(city, city, population)
        ).collect(Collectors.toSet());
    }


}
