package com.anodot.weather.dto;

import com.anodot.weather.weatherSdk.external.dto.City;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import static com.anodot.weather.utils.DoubleUtils.DOUBLE_FORMAT;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class AggregatedTemperature {

    private City city;
    private double temperature;

    @Override
    public String toString() {
        return String.format("%s | %s", city.getName(), DOUBLE_FORMAT.format(temperature));
    }
}
