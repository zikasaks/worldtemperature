package com.anodot.weather.dto;

import com.anodot.weather.weatherSdk.external.dto.City;
import com.anodot.weather.weatherSdk.external.dto.DailyTemp;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class CityData {

    private City cityId;
    private List<DailyTemp> temps;

}
