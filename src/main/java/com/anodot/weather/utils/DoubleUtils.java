package com.anodot.weather.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.text.DecimalFormat;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DoubleUtils {

    public static final DecimalFormat DOUBLE_FORMAT = new DecimalFormat("0.00#");
}
