package com.anodot.weather.exceptions;

public class NoSuchAggregationException extends RuntimeException {
    public NoSuchAggregationException(String message) {
        super(message);
    }
}
