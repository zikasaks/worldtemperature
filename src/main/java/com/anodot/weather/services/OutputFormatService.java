package com.anodot.weather.services;

import com.anodot.weather.dto.AggregatedTemperature;

import java.util.List;
import java.util.stream.Collectors;

public class OutputFormatService {

    public String getStdOutString(List<AggregatedTemperature> temperatureList) {
        return temperatureList.stream()
                .map(AggregatedTemperature::toString)
                .collect(Collectors.joining("\n"));
    }
}
