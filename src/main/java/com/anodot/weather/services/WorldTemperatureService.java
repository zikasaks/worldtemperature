package com.anodot.weather.services;

import com.anodot.weather.aggregations.Aggregation;
import com.anodot.weather.aggregations.AggregationFactory;
import com.anodot.weather.config.Config;
import com.anodot.weather.dto.AggregatedTemperature;
import com.anodot.weather.weatherSdk.external.WeatherAPI;
import com.anodot.weather.weatherSdk.external.dto.City;
import com.anodot.weather.weatherSdk.external.dto.DailyTemp;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Log4j2
public class WorldTemperatureService {

    private final Config config;
    private final ExecutorService executorService;
    private final WeatherAPI weatherAPI;

    private static final int MIN_POPULATION = 50000;

    public WorldTemperatureService(Config config,
                                   WeatherAPI weatherAPI,
                                   ExecutorService executorService) {
        this.config = config;
        this.executorService = executorService;
        this.weatherAPI = weatherAPI;
    }

    public List<AggregatedTemperature> getTopAggregatedTemperatures() throws ExecutionException, InterruptedException {
        Set<City> allCitiesByIds = weatherAPI.getAllCitiesByIds(config.getIds());
        List<Future<AggregatedTemperature>> futures = allCitiesByIds
                .stream()
                .filter(city -> city.getPopulation() >= MIN_POPULATION)
                .map(this::getAggregatedTemperatureFuture)
                .collect(Collectors.toList());

        List<AggregatedTemperature> aggregatedTemperatures = getAggregatedTemperatureList(futures);

        return aggregatedTemperatures.stream()
                .sorted(Comparator.comparingDouble(AggregatedTemperature::getTemperature).reversed())
                .limit(3)
                .collect(Collectors.toList());
    }

    private Future<AggregatedTemperature> getAggregatedTemperatureFuture(City city) {
       return executorService.submit(() -> getAggregatedTemperature(city));
    }

    private AggregatedTemperature getAggregatedTemperature(City city) {
        List<DailyTemp> lastYearTemperature = weatherAPI.getLastYearTemperature(city.getId());
        String selectedAggregation = config.getAggregation();
        Aggregation aggregation = AggregationFactory.getAggregation(selectedAggregation);
        lastYearTemperature.forEach(aggregation::aggregate);
        return new AggregatedTemperature(city, aggregation.getAggregatedValue());
    }

    private List<AggregatedTemperature> getAggregatedTemperatureList(List<Future<AggregatedTemperature>> futures) throws ExecutionException, InterruptedException {
        List<AggregatedTemperature> aggregatedTemperatures = new ArrayList<>();
        for (Future<AggregatedTemperature> future : futures) {
            AggregatedTemperature aggregatedTemperature = future.get();
            aggregatedTemperatures.add(aggregatedTemperature);
        }
        return aggregatedTemperatures;
    }

}
