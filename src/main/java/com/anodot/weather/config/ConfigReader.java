package com.anodot.weather.config;

import com.anodot.weather.exceptions.ConfigurationException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.cli.*;

import java.util.HashSet;
import java.util.List;

import static com.anodot.weather.Constants.*;
import static com.anodot.weather.config.Config.DEFAULT_THREADS;

@Log4j2
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ConfigReader {

    //Read command line arguments. Throws ConfigurationException if arguments are wrong
    public static Config parseCommandLine(String[] args) throws ConfigurationException {
       Options options = getOptions();

        CommandLineParser parser = new DefaultParser();
        try {
            CommandLine parse = parser.parse(options, args);
            String[] ids = parse.getOptionValues(CITIES_IDS_PARAMETER);
            String aggregation = getOptionValue(parse, AGGREGATION_PARAMETER, null);
            int threads = Integer.parseInt(getOptionValue(parse, THREADS_PARAMETER, DEFAULT_THREADS));

            return new Config(new HashSet<>(List.of(ids)), aggregation, threads);
        } catch (ParseException e) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp(APPLICATION_NAME, options);
            log.error(e.getMessage(), e);
            throw new ConfigurationException(e.getMessage(), e);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new ConfigurationException(e.getMessage(), e);
        }

    }

    private static Options getOptions() {
        Options options = new Options();

        Option cityIds = new Option("i", CITIES_IDS_PARAMETER, true, "set of city ids");
        cityIds.setValueSeparator(',');
        cityIds.setRequired(true);
        options.addOption(cityIds);

        Option aggregationType = new Option("a", AGGREGATION_PARAMETER, true, "aggregation type");
        aggregationType.setRequired(true);
        options.addOption(aggregationType);

        Option threads = new Option("t", THREADS_PARAMETER, true, "number of parallel threads");
        threads.setRequired(false);
        options.addOption(threads);

        return options;
    }

    private static String getOptionValue(CommandLine commandLine, String option, String defaultValue) {
        String optionValue = commandLine.getOptionValue(option);
        if (optionValue == null) return defaultValue;
        return optionValue;
    }
}
