package com.anodot.weather.config;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
public class Config {

    public static final String DEFAULT_THREADS = "4";

    private Set<String> ids;
    private String aggregation;
    private int threads;
}
