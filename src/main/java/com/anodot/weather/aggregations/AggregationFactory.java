package com.anodot.weather.aggregations;

import com.anodot.weather.exceptions.NoSuchAggregationException;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class AggregationFactory {

    public static Aggregation getAggregation(String aggregationType) {
        switch (aggregationType) {
            case "max":
                return new MaximumAggregation();
            default:
                throw new NoSuchAggregationException("No such aggregation type");
        }
    }
}
