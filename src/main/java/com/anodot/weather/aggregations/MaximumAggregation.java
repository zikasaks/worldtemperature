package com.anodot.weather.aggregations;

import com.anodot.weather.weatherSdk.external.dto.DailyTemp;

public class MaximumAggregation implements Aggregation {

    private double maxValue = Double.MIN_VALUE;

    @Override
    public void aggregate(DailyTemp dailyTemp) {
        double temperature = dailyTemp.getTemperature();
        if (maxValue < temperature) maxValue = temperature;
    }

    @Override
    public double getAggregatedValue() {
        return maxValue;
    }
}
