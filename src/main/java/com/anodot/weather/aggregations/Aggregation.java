package com.anodot.weather.aggregations;

import com.anodot.weather.weatherSdk.external.dto.DailyTemp;

public interface Aggregation {

    void aggregate(DailyTemp dailyTemp);

    double getAggregatedValue();
}
