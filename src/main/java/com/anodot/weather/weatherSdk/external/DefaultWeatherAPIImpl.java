package com.anodot.weather.weatherSdk.external;

import com.anodot.weather.weatherSdk.external.dto.City;
import com.anodot.weather.weatherSdk.external.dto.DailyTemp;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

//implementation returning fake data
public class DefaultWeatherAPIImpl implements WeatherAPI {
    @Override
    public Set<City> getAllCitiesByIds(Set<String> cityIds) {
        return cityIds.stream()
                .map((id) -> new City(id, id, 534334))
                .collect(Collectors.toSet());
    }

    @Override
    public List<DailyTemp> getLastYearTemperature(String cityId) {
        return List.of(new DailyTemp(new Date(), 10),
                new DailyTemp(new Date(), 43.5));
    }
}
