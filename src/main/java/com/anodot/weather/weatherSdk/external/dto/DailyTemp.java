package com.anodot.weather.weatherSdk.external.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Date;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class DailyTemp {
    private Date date;
    private double temperature;

}
