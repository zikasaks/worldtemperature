package com.anodot.weather.weatherSdk.external.dto;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@AllArgsConstructor
@EqualsAndHashCode
public class City {
    private String id;
    private String name;
    private int population;

}
