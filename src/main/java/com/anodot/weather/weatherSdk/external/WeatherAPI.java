package com.anodot.weather.weatherSdk.external;

import com.anodot.weather.weatherSdk.external.dto.City;
import com.anodot.weather.weatherSdk.external.dto.DailyTemp;

import java.util.List;
import java.util.Set;

public interface WeatherAPI {
    Set<City> getAllCitiesByIds(Set<String> cityIds);
    List<DailyTemp> getLastYearTemperature(String cityId);
}
