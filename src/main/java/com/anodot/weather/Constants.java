package com.anodot.weather;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

    public static String APPLICATION_NAME = "World Temperature";
    public static String AGGREGATION_PARAMETER = "aggregation";
    public static String CITIES_IDS_PARAMETER = "ids";
    public static String THREADS_PARAMETER = "threads";
}
