package com.anodot;

import com.anodot.weather.config.Config;
import com.anodot.weather.config.ConfigReader;
import com.anodot.weather.dto.AggregatedTemperature;
import com.anodot.weather.exceptions.ConfigurationException;
import com.anodot.weather.services.OutputFormatService;
import com.anodot.weather.services.WorldTemperatureService;
import com.anodot.weather.weatherSdk.external.DefaultWeatherAPIImpl;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) throws ConfigurationException {
        Config config = ConfigReader.parseCommandLine(args);
        ExecutorService executorService = Executors.newFixedThreadPool(config.getThreads());
        WorldTemperatureService worldTemperatureService = new WorldTemperatureService(config, new DefaultWeatherAPIImpl(), executorService);

        try {
            List<AggregatedTemperature> topAggregatedTemperatures = worldTemperatureService.getTopAggregatedTemperatures();
            OutputFormatService outputFormatService = new OutputFormatService();
            String stdOutString = outputFormatService.getStdOutString(topAggregatedTemperatures);
            System.out.println(stdOutString);
        } catch (ExecutionException | InterruptedException ex) {
            String error = String.format("Can't get aggregated temperature: %s", ex.getCause().getMessage());
            System.err.println(error);
        }
        executorService.shutdown();
    }
}
