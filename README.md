# World Temperature Test Program

Program for show code quality. 

## Command Line arguments

| Argument description         | Short | Full        | Possible values    | Argument example |
|------------------------------|-------|-------------|--------------------|------------------|
| List of cities' id           | i     | ids         | any                | -i TLV,MSK       |
| Aggregation type             | a     | aggregation | max                | -a max           |
| Number of parallell threads  | t     | threads     | positive integer   | -t 10            |  